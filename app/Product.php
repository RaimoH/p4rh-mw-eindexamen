<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function category() {
        return $this->hasOne('App\Category', 'id', 'IdCategory'); //Een product heeft 1 category, id is de column in de table Categories waarnaar de foreign key IdCategory uit de table Products verwijst
    }
}
