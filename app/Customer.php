<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public function country() {
        return $this->hasOne('App\Country', 'id', 'IdCountry'); //Een customer heeft 1 country, id is de column in de table Countries waarnaar de foreign key IdCountry uit de table Customers verwijst
    }
}
