<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Country;

class CountryController extends Controller
{
    public function index() {
        $countries = Country::all();
        
        return view('countries.index', array('countries'=> $countries));
    }
    
    public function create() {
        $countries = Country::all();
        
        return view('countries.create', array('countries'=> $countries));
    }
    
    public function store(Request $request) {
        $this->validate($request, [
            'code' => 'max:2|required|unique:countries',  //Max 2 chars, required input and must be unique in countries table
            'name' => 'max:255|required|unique:countries', //Max 255 chars, required input and must be unique in countries table
            'latitude' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/', //Multiple numerals before decimal point, ONLY 2 after decimal point
            'longitude' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/', //Multiple numerals before decimal point, ONLY 2 after decimal point
            'shippingCostMultiplier' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/', //Multiple numerals before decimal point, ONLY 2 after decimal point
        ]);
        
        $country = new Country;
        $country->Code = $request->input('code');
        $country->Name = $request->input('name');
        $country->Latitude = empty($request->input('latitude')) ? null : $request->input('latitude');
        $country->Longitude = empty($request->input('longitude')) ? null : $request->input('longitude');
        $country->ShippingCostMultiplier = empty($request->input('shippingCostMultiplier')) ? null : $request->input('shippingCostMultiplier');
        $country->save();
        
        return redirect('/countries');
    }
    public function show($id) {
        $country = Country::find($id);
        $countries = Country::all();
        
        return view('countries.show', array('country' => $country, 'countries' => $countries));
    }
     public function edit($id) {
        $country = Country::find($id);
        $countries = Country::all();
        
        return view('countries.edit', array('country' => $country, 'countries' => $countries));
    }
    
    public function update(Request $request, $id) {
        $this->validate($request, [
            'code' => 'required|unique:countries,code,'.$id.',id|max:2', //required field, must be unique in the Countries table except for the row of current id, max 2
            'name' => 'required|unique:countries,name,'.$id.',id|max:255', //requiured field, must be unique in the Countries table except for the row of current id, max 255
            'latitude' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/', //Multiple numerals before decimal point, ONLY 2 after decimal point
            'longitude' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/', //Multiple numerals before decimal point, ONLY 2 after decimal point
            'shippingCostMultiplier' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/', //Multiple numerals before decimal point, ONLY 2 after decimal point
        ]);
        
        $country = Country::find($id);
        $country->Code = $request->input('code');
        $country->Name = $request->input('name');
        $country->Latitude = empty($request->input('latitude')) ? null : $request->input('latitude');
        $country->Longitude = empty($request->input('longitude')) ? null : $request->input('longitude');
        $country->ShippingCostMultiplier = empty($request->input('shippingCostMultiplier')) ? null : $request->input('shippingCostMultiplier');
        $country->save();
        
        return redirect('/countries/'.$id);
    }
    
    public function destroy($id) {
        $country = Country::find($id);
        $country->delete(); 
        
        return redirect('/countries');
    }
}