<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();
        
        return view('products.index', array('products'=> $products));
    }

    public function create()
    {
        $products = Product::all();
        $categories = Category::all();
        
        return view('products.create', array('products'=> $products, 'categories' => $categories));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'description' => 'max:1024',
            'name' => 'required|max:255', //Required field
            'price' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/', //Multiple numerals before decimal point, ONLY 2 after decimal point
            'shippingCost' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/', //Multiple numerals before decimal point, ONLY 2 after decimal point
            'totalRating' => 'max:10|regex:/^[0-9]+?$/', //Multiple numerals
            'thumbnail' => 'max:255',
            'image' => 'max:255',
            'discountPercentage' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/', //Multiple numerals before decimal point, ONLY 2 after decimal point
            'votes' => 'max:11|regex:/^[0-9]+?$/', //Multiple numerals
            'idCategory' => 'required|max:10', //Required field
        ]);
        
        $product = new Product;
        $product->Description = empty($request->input('description')) ? null : $request->input('description');
        $product->Name = $request->input('name');
        $product->Price = empty($request->input('price')) ? null : $request->input('price');
        $product->ShippingCost = empty($request->input('shippingCost')) ? null : $request->input('shippingCost');
        $product->TotalRating = empty($request->input('totalRating')) ? null : $request->input('totalRating');
        $product->Thumbnail = empty($request->input('thumbnail')) ? null : $request->input('thumbnail');
        $product->Image = empty($request->input('image')) ? null : $request->input('image');
        $product->DiscountPercentage = empty($request->input('discountPercentage')) ? null : $request->input('discountPercentage');
        $product->Votes = empty($request->input('votes')) ? null : $request->input('votes');
        $product->IdCategory = $request->input('idCategory');
        $product->save();
        
        return redirect('/products');
    }

    public function show($id)
    {
        $product = Product::find($id);
        $products = Product::all();
        $category = Category::find($product->IdCategory);
        
        return view('products.show', array('product' => $product,'category' => $category, 'products' => $products));
    }

    public function edit($id)
    {
        $product = Product::find($id);
        $category = Category::find($product->IdCategory);
        $products = Product::all();
        $categories = Category::all();
        
        return view('products.edit', array('product' => $product, 'category' => $category, 'products' => $products, 'categories' => $categories));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'description' => 'max:1024',
            'name' => 'required|max:255', //Required field
            'price' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/', //Multiple numerals before decimal point, ONLY 2 after decimal point
            'shippingCost' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/', //Multiple numerals before decimal point, ONLY 2 after decimal point
            'totalRating' => 'max:10|regex:/^[0-9]+?$/', //Multiple numerals
            'thumbnail' => 'max:255',
            'image' => 'max:255',
            'discountPercentage' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/', //Multiple numerals before decimal point, ONLY 2 after decimal point
            'votes' => 'max:11|regex:/^[0-9]+?$/', //Multiple numerals
            'idCategory' => 'required|max:10', //Required field
        ]);
        
        $product = Product::find($id);
        $product->Description = empty($request->input('description')) ? null : $request->input('description');
        $product->Name = $request->input('name');
        $product->Price = empty($request->input('price')) ? null : $request->input('price');
        $product->ShippingCost = empty($request->input('shippingCost')) ? null : $request->input('shippingCost');
        $product->TotalRating = empty($request->input('totalRating')) ? null : $request->input('totalRating');
        $product->Thumbnail = empty($request->input('thumbnail')) ? null : $request->input('thumbnail');
        $product->Image = empty($request->input('image')) ? null : $request->input('image');
        $product->DiscountPercentage = empty($request->input('discountPercentage')) ? null : $request->input('discountPercentage');
        $product->Votes = empty($request->input('votes')) ? null : $request->input('votes');
        $product->IdCategory = $request->input('idCategory');
        $product->save();
        
        return redirect('/products/'.$id);
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete(); 
        
        return redirect('/products');
    }
}
