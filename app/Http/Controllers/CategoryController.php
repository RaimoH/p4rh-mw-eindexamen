<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;

class CategoryController extends Controller
{
     public function index() {
        $categories = Category::all();
        
        return view('categories.index', array('categories'=> $categories));
    }
    
    public function create() {
        $categories = Category::all();
        
        return view('categories.create', array('categories'=> $categories));
    }

    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'max:255|required|unique:categories',  // max 255, required field and must be unique in the Categories table
            'description' => 'max:1024', // max 1024
        ]);
        
        $category = new Category;
        $category->Name = $request->input('name');
        $category->Description = empty($request->input('description')) ? null : $request->input('description');
        $category->save();
        
        return redirect('/categories');
    }

    public function show($id) {
        $category = Category::find($id);
        $categories = Category::all();
        
        return view('categories.show', array('category' => $category, 'categories' => $categories));
    }

    public function edit($id) {
        $category = Category::find($id);
        $categories = Category::all();
        
        return view('categories.edit', array('category' => $category, 'categories' => $categories));
  
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required|unique:categories,name,'.$id.',id|max:255',  //max 255, required field and must be unique in the Categories table, in the column 'name', except for the name from the current id.
            'description' => 'max:1024', //max 1024
        ]);
        
        $category = Category::find($id);
        $category->Name = $request->input('name');
        $category->Description = empty($request->input('description')) ? null : $request->input('description');
        $category->save();
        
        return redirect('/categories/'.$id);
    }
    
    public function destroy($id) {
        $category = Category::find($id);
        $category->delete(); 
        
        return redirect('/categories');
    }
}
