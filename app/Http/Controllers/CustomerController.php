<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Customer;
use App\Country;

class CustomerController extends Controller
{
    public function index() {
        $customers = Customer::all();
        
        return view('customers.index', array('customers' => $customers));
    }

    public function create(){
        $customers = Customer::all();
        $countries = Country::all();
        
        return view ('customers.create', array('customers' => $customers,'countries'=> $countries));
    }
    public function store(Request $request){
         $this->validate($request, [
            'nickName' => 'max:10|required', //Required field
            'firstName' => 'max:255|required', //Required field
            'lastName' => 'max:255|required', //Required field
            'address1' => 'max:255|required', //Required field
            'address2' => 'max:255',
            'city' => 'max:255|required', //Required field
            'region' => 'max:80',
            'postalCode' => 'max:20|required', //Required field
            'phone' => 'max:40',
            'mobile' => 'max:40',
            'idCountry' => 'max:10|required', //Required field
        ]);
        
        $customer = new Customer;
        $customer->NickName = $request->input('nickName');
        $customer->FirstName = $request->input('firstName');
        $customer->LastName = $request->input('lastName');
        $customer->Address1 = $request->input('address1');
        $customer->Address2 = empty($request->input('address2')) ? null : $request->input('address2');
        $customer->City = $request->input('city');
        $customer->Region = empty($request->input('region')) ? null : $request->input('region');
        $customer->PostalCode = $request->input('postalCode');
        $customer->Phone = empty($request->input('phone')) ? null : $request->input('phone');
        $customer->Mobile = empty($request->input('mobile')) ? null : $request->input('mobile');
        $customer->IdCountry = $request->input('idCountry');
        $customer->save();
        
        return redirect('/customers');
    }
    public function show($id)
    {
        $customer = Customer::find($id);
        $customers = Customer::all();
        
        return view('customers.show', array('customer' => $customer, 'customers' => $customers));
    }
    
    public function edit($id)
    {
        $customer = Customer::find($id);
        $customers = Customer::all();
        $country = Country::find($customer->IdCountry);
        $countries = Country::all();
        
        return view('customers.edit', array('customer' => $customer, 'customers' => $customers, 'country' => $country, 'countries' => $countries));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nickName' => 'max:10|required', //Required field
            'firstName' => 'max:255|required', //Required field
            'lastName' => 'max:255|required', //Required field
            'address1' => 'max:255|required', //Required field
            'address2' => 'max:255',
            'city' => 'max:255|required', //Required field
            'region' => 'max:80',
            'postalCode' => 'max:20|required', //Required field
            'phone' => 'max:40',
            'mobile' => 'max:40',
            'idCountry' => 'max:10|required', //Required field
        ]);
        
        $customer = Customer::find($id);
        $customer->NickName = $request->input('nickName');
        $customer->FirstName = $request->input('firstName');
        $customer->LastName = $request->input('lastName');
        $customer->Address1 = $request->input('address1');
        $customer->Address2 = empty($request->input('address2')) ? null : $request->input('address2');
        $customer->City = $request->input('city');
        $customer->Region = empty($request->input('region')) ? null : $request->input('region');
        $customer->PostalCode = $request->input('postalCode');
        $customer->Phone = empty($request->input('phone')) ? null : $request->input('phone');
        $customer->Mobile = empty($request->input('mobile')) ? null : $request->input('mobile');
        $customer->IdCountry = $request->input('idCountry');
        $customer->save();
        
        return redirect('/customers/'.$id);
    }
    
    public function destroy($id) {
        $customer = Customer::find($id);
        $customer->delete(); 
        
        return redirect('/customers');
    }
}
