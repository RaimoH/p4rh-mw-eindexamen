@extends('layouts.crud')

@section('content')
    <form method="POST" action="{{ action('CustomerController@update', $customer->id) }}">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h2 class="pull-left">Customers</h2>
                        <nav class="pull-right">
                            <button type="submit" class="btn btn-success btn-lg">Update</button>
                            <a href="/customers/{{ $customer->id }}"><button type="button" class="btn btn-danger btn-lg">Cancel</button></a>
                        </nav>
                    </div>
                </div>
                
                <input type="hidden" name="_method" value="PUT"/>
                {{ csrf_field() }} <!-- builds the entire input field -->
                
                <div class="wrapper panel panel-default">
                    <div class="row panel-body">
                        <div class="form-group col-md-4">
                            <label for="firstName">Voornaam:</label>
                            <input type="text" name="firstName" id="firstName" class="form-control" value="{{ $customer->FirstName }}"/>
                        </div>
                        
                        <div class="form-group col-md-8">
                            <label for="lastName">Familienaam:</label>
                            <input type="text" name="lastName" id="lastName" class="form-control" value="{{ $customer->LastName }}"/>
                        </div>
                        
                        <div class="form-group col-md-12">
                            <label for="nickName">Roepnaam:</label>
                            <input type="text" name="nickName" id="nickName" class="form-control" value="{{ $customer->NickName }}"/>
                        </div>
                        
                        <div class="form-group col-md-12">
                            <label for="address1">Adres 1:</label>
                            <input type="text" name="address1" id="address1" class="form-control" value="{{ $customer->Address1 }}"/>
                        </div>
                        
                        <div class="form-group col-md-12">
                            <label for="address2">Adres 2:</label>
                            <input type="text" name="address2" id="address2" class="form-control" value="{{ $customer->Address2 }}"/>
                        </div>
                        
                        <div class="form-group col-md-2">
                            <label for="postalCode">Postcode:</label>
                            <input type="text" name="postalCode" id="postalCode" class="form-control" value="{{ $customer->PostalCode }}"/>
                        </div>
                        <div class="form-group col-md-10">
                            <label for="city">Stad:</label>
                            <input type="text" name="city" id="city" class="form-control" value="{{ $customer->City }}"/>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="region">Regio:</label>
                            <input type="text" name="region" id="region" class="form-control" value="{{ $customer->Region }}"/>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="idCountry">Land:</label>
                            <select name="idCountry" id="idCountry" class="form-control">
                                @foreach($countries as $var)
                                    <option value="{{ $var->id }}" @if($var->id == $country->id) selected='selected' @endif>{{ $var->Name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="phone">Telefoon:</label>
                            <input type="text" name="phone" id="phone" class="form-control" value="{{ $customer->Phone }}"/>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="mobile">Mobieltje:</label>
                            <input type="text" name="mobile" id="mobile" class="form-control" value="{{ $customer->Mobile }}"/>
                        </div>
                    </div>
                </div>
                @if (count($errors) > 0) <!-- https://laravel.com/docs/5.3/validation#quick-displaying-the-validation-errors -->
                    <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    </div>
                @endif
            </div>
            
            <div class="col-md-4 table-responsive">
                <table class="table table-bordered table-striped">
                    <tr>
                        <th></th>
                        <th>Id</th>
                        <th>Roepnaam</th>
                        <th>Voornaam</th>
                        <th>Familienaam</th>
                        <th>Stad</th>
                        <th>Land</th>
                    </tr>
                    
                    @foreach($customers as $customer)
                        <tr>
                            <td><a href="/customers/{{ $customer->id }}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                            <td>{{ $customer->id }}</td>
                            <td>{{ $customer->NickName }}</td>
                            <td>{{ $customer->FirstName }}</td>
                            <td>{{ $customer->LastName }}</td>
                            <td>{{ $customer->City }}</td>
                            <td>{{ $customer->country->Name }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </form>
@endsection