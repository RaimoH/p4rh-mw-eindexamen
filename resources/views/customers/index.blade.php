@extends('layouts.crud')

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h2 class="pull-left">Customers</h2>
                    <nav class="pull-right">
                        <a href="/customers/create"><button type="button" class="btn btn-primary btn-lg">Inserting</button></a>
                    </nav>
                </div>
            </div>
        </div>
        
        <div class="col-md-4 table-responsive">
            <table class="table table-bordered table-striped">
                <tr>
                    <th></th>
                    <th>Id</th>
                    <th>Roepnaam</th>
                    <th>Voornaam</th>
                    <th>Familienaam</th>
                    <th>Stad</th>
                    <th>Land</th>
                </tr>
                
                @foreach($customers as $customer)
                    <tr>
                        <td><a href="/customers/{{ $customer->id }}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                        <td>{{ $customer->id }}</td>
                        <td>{{ $customer->NickName }}</td>
                        <td>{{ $customer->FirstName }}</td>
                        <td>{{ $customer->LastName }}</td>
                        <td>{{ $customer->City }}</td>
                        <td>{{ $customer->country->Name }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection