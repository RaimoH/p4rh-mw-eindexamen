@extends('layouts.crud')

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h2 class="pull-left">Customers</h2>
                    <nav class="pull-right">
                        <form method="POST" action="{{ action('CustomerController@destroy', $customer->id) }}">
                            <input type="hidden" name="_method" value="DELETE"/>
                            {{ csrf_field() }}
                            <a href="/customers/{{ $customer->id }}/edit"><button type="button" class="btn btn-primary btn-lg">Updating</button>
                            <a href="{{ action('CustomerController@create') }}"><button type="button" class="btn btn-info btn-lg">Inserting</button></a>
                            <button type="submit" class="btn btn-warning btn-lg">Delete</button>
                            <a href="{{ action('CustomerController@index') }}"><button type="button" class="btn btn-danger btn-lg">Cancel</button></a>
                        </form>
                    </nav>
                </div>
            </div>
            
            <div class="wrapper panel panel-default">
                <div class="row panel-body">
                    <div class="form-group col-md-4">
                        <label for="firstName">Voornaam:</label>
                        <input type="text" name="firstName" id="firstName" class="form-control" value="{{ $customer->FirstName }}" readonly/>
                    </div>
                    <div class="form-group col-md-8">
                        <label for="lastName">Familienaam:</label>
                        <input type="text" name="lastName" id="lastName" class="form-control" value="{{ $customer->LastName }}" readonly/>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="nickName">Roepnaam:</label>
                        <input type="text" name="nickName" id="nickName" class="form-control" value="{{ $customer->NickName }}" readonly/>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="address1">Adres 1:</label>
                        <input type="text" name="address1" id="address1" class="form-control" value="{{ $customer->Address1 }}" readonly/>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="address2">Adres 2:</label>
                        <input type="text" name="address2" id="address2" class="form-control" value="{{ $customer->Address2 }}" readonly/>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="postalCode">Postcode:</label>
                        <input type="text" name="postalCode" id="postalCode" class="form-control" value="{{ $customer->PostalCode }}" readonly/>
                    </div>
                    <div class="form-group col-md-10">
                        <label for="city">Stad:</label>
                        <input type="text" name="city" id="city" class="form-control" value="{{ $customer->City }}" readonly/>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="region">Regio:</label>
                        <input type="text" name="region" id="region" class="form-control" value="{{ $customer->Region }}" readonly/>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="idCountry">Land:</label>
                        <input type="text" name="idCountry" id="idCountry" class="form-control" value="{{ $customer->country->Name }}" readonly/>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="phone">Telefoon:</label>
                        <input type="text" name="phone" id="phone" class="form-control" value="{{ $customer->Phone }}" readonly/>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="mobile">Mobieltje:</label>
                        <input type="text" name="mobile" id="mobile" class="form-control" value="{{ $customer->Mobile }}" readonly/>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-md-4 table-responsive">
            <table class="table table-bordered table-striped">
                <tr>
                    <th></th>
                    <th>Id</th>
                    <th>Roepnaam</th>
                    <th>Voornaam</th>
                    <th>Familienaam</th>
                    <th>Stad</th>
                    <th>Land</th>
                </tr>
                
                @foreach($customers as $customer)
                    <tr>
                        <td><a href="/customers/{{ $customer->id }}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                        <td>{{ $customer->id }}</td>
                        <td>{{ $customer->NickName }}</td>
                        <td>{{ $customer->FirstName }}</td>
                        <td>{{ $customer->LastName }}</td>
                        <td>{{ $customer->City }}</td>
                        <td>{{ $customer->country->Name }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection