@extends ('layouts.management')

@section('content')
    <div class="row">
        <div class="col-md-3">
            <a href="/products"><button type="button" class="tile btn btn-default">Products</button></a>
        </div>
        <div class="col-md-3">
            <a href="/categories"><button type="button" class="tile btn btn-default">Categories</button></a>
        </div>
        <div class="col-md-6">
            <div class="tile well well-lg">&nbsp;</div>
        </div>
        
        <div class="col-md-3">
            <a href="/customers"><button type="button" class="tile btn btn-default">Customers</button></a>
        </div>
        <div class="col-md-3">
            <div class="tile well well-lg">&nbsp;</div>
        </div>
        <div class="col-md-3">
            <button type="button" class="tile btn btn-default disabled">Order</button>
        </div>
        <div class="col-md-3">
            <button type="button" class="tile btn btn-default disabled">Order Items</button>
        </div>
        
        <div class="col-md-3">
            <div class="tile well well-lg">&nbsp;</div>
        </div>
        <div class="col-md-3">
            <a href="/countries"><button type="button" class="tile btn btn-default">Countries</button></a>
        </div>
        <div class="col-md-3">
            <button type="button" class="tile btn btn-default disabled">Order Status</button>
        </div>
        <div class="col-md-3">
            <button type="button" class="tile btn btn-default disabled">Unit Base</button>
        </div>
    </div>
@endsection