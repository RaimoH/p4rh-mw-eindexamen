<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Mikmak Webwinkel</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
    <header>
        <nav class="navbar navbar-inverse container-fluid">
            <div class="navbar-header pull-right">
                <a class="navbar-brand" href="/">Mikmak</a>
            </div>
        </nav>
    </header>
    
    <main class="container-fluid">
        @yield('content')
    </main>
    
    <footer class="container-fluid navbar navbar-inverse navbar-fixed-bottom">
        <a class="navbar-btn btn btn-default pull-left" href="/admin">Beheer</a>
        <div class="navbar-text pull-right">&copy; Made by Maarten Weyn &amp; Raimo Huybrechts</div>
    </footer>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>