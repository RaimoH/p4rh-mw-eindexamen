<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Mikmak Webwinkel - CRUD</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css" />
</head>
<body>
    <header>
        <nav class="navbar navbar-inverse container-fluid">
            <div class="nav navbar-nav">
                <a href="/admin"><button type="button" class="btn btn-default navbar-btn" aria-label="Menu">
                    <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
                </button></a>
            </div>
            <div class="navbar-header pull-right">
                <a class="navbar-brand" href="/">Mikmak</a>
            </div>
        </nav>
    </header>
    
    <main class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-body">
                @yield('content')
            </div>
        </div>
    </main>
    
    <footer class="container-fluid navbar navbar-inverse navbar-fixed-bottom">
        <div class="navbar-text pull-right">&copy; Made by Maarten Weyn &amp; Raimo Huybrechts</div>
    </footer>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>