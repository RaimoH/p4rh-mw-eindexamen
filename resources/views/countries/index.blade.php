@extends('layouts.crud')

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h2 class="pull-left">Countries</h2>
                    <nav class="pull-right">
                        <a href="/countries/create"><button type="button" class="btn btn-primary btn-lg">Inserting</button></a>
                    </nav>
                </div>
            </div>
        </div>
        
        <div class="col-md-4 table-responsive">
            <table class="table table-bordered table-striped">
                <tr>
                    <th></th>
                    <th>Id</th>
                    <th>Code</th>
                    <th>Naam</th>
                    <th>Verzendkosten</th>
                </tr>
                
                @foreach($countries as $country)
                    <tr>
                        <td><a href="/countries/{{ $country->id }}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                        <td>{{ $country->id }}</td>
                        <td>{{ $country->Code }}</td>
                        <td>{{ $country->Name }}</td>
                        <td>{{ $country->ShippingCostMultiplier }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection