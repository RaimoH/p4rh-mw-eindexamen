@extends('layouts.crud')

@section('content')
    <form method="POST" action="{{ action('CountryController@store') }}">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h2 class="pull-left">Countries</h2>
                        <nav class="pull-right">
                            <button type="submit" class="btn btn-success btn-lg">Create</button>
                            <a href="{{ action('CountryController@index') }}"><button type="button" class="btn btn-danger btn-lg">Cancel</button></a>
                        </nav>
                    </div>
                </div>
                
                {{ csrf_field() }} <!-- builds the entire input field -->
                
                <div class="wrapper panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="code">Code:</label>
                            <input type="text" name="code" id="code" class="form-control" value="{{ old('code') }}"/>
                        </div>
                        <div class="form-group">
                            <label for="name">Naam:</label>
                            <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}"/>
                        </div>
                        <div class="form-group">
                            <label for="latitude">Breedtegraad:</label>
                            <input type="text" name="latitude" id="latitude" class="form-control" value="{{ old('latitude') }}"/>
                        </div>
                        <div class="form-group">
                            <label for="longitude">Lengtegraad:</label>
                            <input type="text" name="longitude" id="longitude" class="form-control" value="{{ old('longitude') }}"/>
                        </div>
                        <div class="form-group">
                            <label for="shippingCostMultiplier">Verzendkosten:</label>
                            <input type="text" name="shippingCostMultiplier" id="shippingCostMultiplier" class="form-control" value="{{ old('shippingCostMultiplier') }}"/>
                        </div>
                    </div>
                </div>
                @if (count($errors) > 0) <!-- https://laravel.com/docs/5.3/validation#quick-displaying-the-validation-errors -->
                    <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    </div>
                @endif
            </div>
            
            <div class="col-md-4 table-responsive">
                <table class="table table-bordered table-striped">
                    <tr>
                        <th></th>
                        <th>Id</th>
                        <th>Code</th>
                        <th>Naam</th>
                        <th>Verzendkosten</th>
                    </tr>
                    
                    @foreach($countries as $country)
                        <tr>
                            <td><a href="/countries/{{ $country->id }}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                            <td>{{ $country->id }}</td>
                            <td>{{ $country->Code }}</td>
                            <td>{{ $country->Name }}</td>
                            <td>{{ $country->ShippingCostMultiplier }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </form>
@endsection