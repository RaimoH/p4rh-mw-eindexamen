@extends('layouts.crud')

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h2 class="pull-left">Countries</h2>
                    <nav class="pull-right">
                        <form method="POST" action="{{ action('CountryController@destroy', $country->id) }}">
                            <input type="hidden" name="_method" value="DELETE"/>
                            {{ csrf_field() }}
                            <a href="/countries/{{ $country->id }}/edit"><button type="button" class="btn btn-primary btn-lg">Updating</button>
                            <a href="{{ action('CountryController@create') }}"><button type="button" class="btn btn-info btn-lg">Inserting</button></a>
                            <button type="submit" class="btn btn-warning btn-lg">Delete</button>
                            <a href="{{ action('CountryController@index') }}"><button type="button" class="btn btn-danger btn-lg">Cancel</button></a>
                        </form>
                    </nav>
                </div>
            </div>
            
            <div class="wrapper panel panel-default">
                <div class="panel-body">
                    <div class="form-group">
                        <label for="code">Code:</label>
                        <input type="text" name="code" id="code" class="form-control" value="{{ $country->Code }}" readonly/>
                    </div>
                    <div class="form-group">
                        <label for="name">Naam:</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ $country->Name }}" readonly/>
                    </div>
                    <div class="form-group">
                        <label for="latitude">Breedtegraad:</label>
                        <input type="text" name="latitude" id="latitude" class="form-control" value="{{ $country->Latitude }}" readonly/>
                    </div>
                    <div class="form-group">
                        <label for="longitude">Lengtegraad:</label>
                        <input type="text" name="longitude" id="longitude" class="form-control" value="{{ $country->Longitude }}" readonly/>
                    </div>
                    <div class="form-group">
                        <label for="shippingCostMultiplier">Verzendkosten:</label>
                        <input type="text" name="shippingCostMultiplier" id="shippingCostMultiplier" class="form-control" value="{{ $country->ShippingCostMultiplier }}" readonly/>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-md-4 table-responsive">
            <table class="table table-bordered table-striped">
                <tr>
                    <th></th>
                    <th>Id</th>
                    <th>Code</th>
                    <th>Naam</th>
                    <th>Verzendkosten</th>
                </tr>
                
                @foreach($countries as $country)
                    <tr>
                        <td><a href="/countries/{{ $country->id }}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                        <td>{{ $country->id }}</td>
                        <td>{{ $country->Code }}</td>
                        <td>{{ $country->Name }}</td>
                        <td>{{ $country->ShippingCostMultiplier }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection