@extends('layouts.crud')

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h2 class="pull-left">Products</h2>
                    <nav class="pull-right">
                        <form method="POST" action="{{ action('ProductController@destroy', $product->id) }}">
                            <input type="hidden" name="_method" value="DELETE"/>
                            {{ csrf_field() }}
                            <a href="/products/{{ $product->id }}/edit"><button type="button" class="btn btn-primary btn-lg">Updating</button>
                            <a href="{{ action('ProductController@create') }}"><button type="button" class="btn btn-info btn-lg">Inserting</button></a>
                            <button type="submit" class="btn btn-warning btn-lg">Delete</button>
                            <a href="{{ action('ProductController@index') }}"><button type="button" class="btn btn-danger btn-lg">Cancel</button></a>
                        </form>
                    </nav>
                </div>
            </div>
            
            <div class="wrapper panel panel-default">
                <div class="row panel-body">
                    <div class="form-group col-md-8">
                        <label for="name">Naam:</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ $product->Name }}" readonly/>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="idCategory">Categorie:</label>
                        <input type="text" name="idCategory" id="idCategory" class="form-control" value="{{ $product->category->Name }}" readonly/>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="description">Beschrijving:</label>
                        <input type="text" name="description" id="description" class="form-control" value="{{ $product->Description }}" readonly/>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="price">Prijs:</label>
                        <input type="text" name="price" id="price" class="form-control" value="{{ $product->Price }}" readonly/>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="shippingCost">Verzendkosten:</label>
                        <input type="text" name="shippingCost" id="shippingCost" class="form-control" value="{{ $product->ShippingCost }}" readonly/>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="discountPercentage">Aanbiedingspercent:</label>
                        <input type="text" name="discountPercentage" id="discountPercentage" class="form-control" value="{{ $product->DiscountPercentage }}" readonly/>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="votes">Stemmen:</label>
                        <input type="text" name="votes" id="votes" class="form-control" value="{{ $product->Votes }}" readonly/>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="totalRating">Totaal rating:</label>
                        <input type="text" name="totalRating" id="totalRating" class="form-control" value="{{ $product->TotalRating }}" readonly/>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="image">Afbeelding:</label>
                        <input type="text" name="image" id="image" class="form-control" value="{{ $product->Image }}" readonly/>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="thumbnail">Miniatuur:</label>
                        <input type="text" name="thumbnail" id="thumbnail" class="form-control" value="{{ $product->Thumbnail }}" readonly/>
                    </div>
                </div>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
                </div>
            @endif
        </div>
            
        <div class="col-md-4 table-responsive">
            <table class="table table-bordered table-striped">
                    <tr>
                        <th></th>
                        <th>Id</th>
                        <th>Naam</th>
                        <th>Prijs</th>
                        <th>Verzendkosten</th>
                        <th>Totaal rating</th>
                        <th>Aanbiedingspercent</th>
                        <th>Stemmen</th>
                        <th>Categorie</th>
                    </tr>
                    
                    @foreach($products as $product)
                        <tr>
                            <td><a href="/products/{{ $product->id }}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                            <td>{{ $product->id }}</td>
                            <td>{{ $product->Name }}</td>
                            <td>{{ empty($product->Price) ? null : number_format($product->Price, 2, ',', '') }}</td> <!--If price is not empty, display price with 2 decimals and a comma instead of a point-->
                            <td>{{ $product->ShippingCost }}</td>
                            <td>{{ $product->TotalRating }}</td>
                            <td>{{ $product->DiscountPercentage }}</td>
                            <td>{{ $product->Votes }}</td>
                            <td>{{ $product->category->Name }}</td>
                        </tr>
                    @endforeach
            </table>
        </div>
    </div>
@endsection