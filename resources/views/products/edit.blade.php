@extends('layouts.crud')

@section('content')
    <form method="POST" action="{{ action('ProductController@update', $product->id) }}">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h2 class="pull-left">Products</h2>
                        <nav class="pull-right">
                            <button type="submit" class="btn btn-success btn-lg">Update</button>
                            <a href="/products/{{ $product->id }}"><button type="button" class="btn btn-danger btn-lg">Cancel</button></a>
                        </nav>
                    </div>
                </div>
                
                <input type="hidden" name="_method" value="PUT"/>
                {{ csrf_field() }} <!-- builds the entire input field -->
                
                <div class="wrapper panel panel-default">
                    <div class="row panel-body">
                        <div class="form-group col-md-8">
                            <label for="name">Naam:</label>
                            <input type="text" name="name" id="name" class="form-control" value="{{ $product->Name }}"/>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="idCategory">Categorie:</label>
                            <select name="idCategory" id="idCategory" class="form-control">
                                @foreach($categories as $var)
                                    <option value="{{ $var->id }}" @if($var->id == $category->id) selected='selected' @endif>{{ $var->Name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="description">Beschrijving:</label>
                            <input type="text" name="description" id="description" class="form-control" value="{{ $product->Description }}"/>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="price">Prijs:</label>
                            <input type="text" name="price" id="price" class="form-control" value="{{ $product->Price }}"/>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="shippingCost">Verzendkosten:</label>
                            <input type="text" name="shippingCost" id="shippingCost" class="form-control" value="{{ $product->ShippingCost }}"/>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="discountPercentage">Aanbiedingspercent:</label>
                            <input type="text" name="discountPercentage" id="discountPercentage" class="form-control" value="{{ $product->DiscountPercentage }}"/>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="votes">Stemmen:</label>
                            <input type="text" name="votes" id="votes" class="form-control" value="{{ $product->Votes }}"/>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="totalRating">Totaal rating:</label>
                            <input type="text" name="totalRating" id="totalRating" class="form-control" value="{{ $product->TotalRating }}"/>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="image">Afbeelding:</label>
                            <input type="text" name="image" id="image" class="form-control" value="{{ $product->Image }}"/>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="thumbnail">Miniatuur:</label>
                            <input type="text" name="thumbnail" id="thumbnail" class="form-control" value="{{ $product->Thumbnail }}"/>
                        </div>
                    </div>
                </div>
                @if (count($errors) > 0) <!-- https://laravel.com/docs/5.3/validation#quick-displaying-the-validation-errors -->
                    <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    </div>
                @endif
            </div>
                
            <div class="col-md-4 table-responsive">
                <table class="table table-bordered table-striped">
                        <tr>
                            <th></th>
                            <th>Id</th>
                            <th>Naam</th>
                            <th>Prijs</th>
                            <th>Verzendkosten</th>
                            <th>Totaal rating</th>
                            <th>Aanbiedingspercent</th>
                            <th>Stemmen</th>
                            <th>Categorie</th>
                        </tr>
                        
                        @foreach($products as $product)
                            <tr>
                                <td><a href="/products/{{ $product->id }}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                                <td>{{ $product->id }}</td>
                                <td>{{ $product->Name }}</td>
                                <td>{{ empty($product->Price) ? null : number_format($product->Price, 2, ',', '') }}</td> <!--If price is not empty, display price with 2 decimals and a comma instead of a point-->
                                <td>{{ $product->ShippingCost }}</td>
                                <td>{{ $product->TotalRating }}</td>
                                <td>{{ $product->DiscountPercentage }}</td>
                                <td>{{ $product->Votes }}</td>
                                <td>{{ $product->category->Name }}</td>
                            </tr>
                        @endforeach
                </table>
            </div>
        </div>
    </form>
@endsection