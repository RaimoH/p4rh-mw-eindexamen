@extends('layouts.crud')

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h2 class="pull-left">Products</h2>
                    <nav class="pull-right">
                        <a href="/products/create"><button type="button" class="btn btn-primary btn-lg">Inserting</button></a>
                    </nav>
                </div>
            </div>
        </div>
        
        <div class="col-md-4 table-responsive">
            <table class="table table-bordered table-striped">
                    <tr>
                        <th></th>
                        <th>Id</th>
                        <th>Naam</th>
                        <th>Prijs</th>
                        <th>Verzendkosten</th>
                        <th>Totaal rating</th>
                        <th>Aanbiedingspercent</th>
                        <th>Stemmen</th>
                        <th>Categorie</th>
                    </tr>
                    
                    @foreach($products as $product)
                        <tr>
                            <td><a href="/products/{{ $product->id }}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                            <td>{{ $product->id }}</td>
                            <td>{{ $product->Name }}</td>
                            <td>{{ empty($product->Price) ? null : number_format($product->Price, 2, ',', '') }}</td> <!--If price is not empty, display price with 2 decimals and a comma instead of a point-->
                            <td>{{ $product->ShippingCost }}</td>
                            <td>{{ $product->TotalRating }}</td>
                            <td>{{ $product->DiscountPercentage }}</td>
                            <td>{{ $product->Votes }}</td>
                            <td>{{ $product->category->Name }}</td>
                        </tr>
                    @endforeach
            </table>
        </div>
    </div>
@endsection