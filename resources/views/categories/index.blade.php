@extends('layouts.crud')

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h2 class="pull-left">Categories</h2>
                    <nav class="pull-right">
                        <a href="/categories/create"><button type="button" class="btn btn-primary btn-lg">Inserting</button></a>
                    </nav>
                </div>
            </div>
        </div>
        
        <div class="col-md-4 table-responsive">
            <table class="table table-bordered table-striped">
                <tr>
                    <th></th>
                    <th>Id</th>
                    <th>Naam</th>
                </tr>
                
                @foreach($categories as $category)
                    <tr>
                        <td><a href="/categories/{{ $category->id }}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                        <td>{{ $category->id }}</td>
                        <td>{{ $category->Name }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection