@extends('layouts.crud')

@section('content')
    <form method="POST" action="{{ action('CategoryController@store') }}">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h2 class="pull-left">Categories</h2>
                        <nav class="pull-right">
                            <button type="submit" class="btn btn-success btn-lg">Create</button>
                            <a href="{{ action('CategoryController@index') }}"><button type="button" class="btn btn-danger btn-lg">Cancel</button></a>
                        </nav>
                    </div>
                </div>
                
                {{ csrf_field() }} <!-- builds the entire input field -->
                
                <div class="wrapper panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="name">Naam:</label>
                            <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}"/>
                        </div>
                        <div class="form-group">
                            <label for="description">Beschrijving:</label>
                            <input type="text" name="description" id="description" class="form-control" value="{{ old('description') }}"/>
                        </div>
                    </div>
                </div>
                @if (count($errors) > 0) <!-- https://laravel.com/docs/5.3/validation#quick-displaying-the-validation-errors -->
                    <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    </div>
                @endif
            </div>
            
            <div class="col-md-4 table-responsive">
                <table class="table table-bordered table-striped">
                    <tr>
                        <th></th>
                        <th>Id</th>
                        <th>Naam</th>
                     
                    </tr>
                    
                    @foreach($categories as $category)
                        <tr>
                            <td><a href="/categories/{{ $category->id }}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                            <td>{{ $category->id }}</td>
                           <td>{{ $category->Name }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </form>
@endsection