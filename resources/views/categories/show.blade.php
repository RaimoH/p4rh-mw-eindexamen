@extends('layouts.crud')

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h2 class="pull-left">Categories</h2>
                    <nav class="pull-right">
                        <form method="POST" action="{{ action('CategoryController@destroy', $category->id) }}">
                            <input type="hidden" name="_method" value="DELETE"/>
                            {{ csrf_field() }}
                            <a href="/categories/{{ $category->id }}/edit"><button type="button" class="btn btn-primary btn-lg">Updating</button>
                            <a href="{{ action('CategoryController@create') }}"><button type="button" class="btn btn-info btn-lg">Inserting</button></a>
                            <button type="submit" class="btn btn-warning btn-lg">Delete</button>
                            <a href="{{ action('CategoryController@index') }}"><button type="button" class="btn btn-danger btn-lg">Cancel</button></a>
                        </form>
                    </nav>
                </div>
            </div>
            
            <div class="wrapper panel panel-default">
                <div class="panel-body">
                    <div class="form-group">
                        <label for="name">Naam:</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ $category->Name }}" readonly/>
                    </div>
                    <div class="form-group">
                        <label for="description">Beschrijving:</label>
                        <input type="text" name="description" id="description" class="form-control" value="{{ $category->Description }}" readonly/>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-md-4 table-responsive">
            <table class="table table-bordered table-striped">
                <tr>
                    <th></th>
                    <th>Id</th>
                    <th>Naam</th>
                </tr>
                
                @foreach($categories as $category)
                    <tr>
                        <td><a href="/categories/{{ $category->id }}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                        <td>{{ $category->id }}</td>
                        <td>{{ $category->Name }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection